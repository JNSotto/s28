//map method
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'GET',
})
.then((response) => response.json())
.then((data)=>{
	let getData = data.map(function(data){
			return data.title
	})
	console.log(getData)
})




//provide the 'title' and 'status' of the to do list item
fetch('https://jsonplaceholder.typicode.com/todos/101',{
		method: 'GET'
	})
	.then((response) => response.json())
	.then((json)=> {
		console.log(json)
		console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)
	})




//create a to do list item using POST method
fetch ('https://jsonplaceholder.typicode.com/todos', {
		method:'POST',
		headers: {
			'Content-Type':'application/json'
		},
		body:JSON.stringify({
			title:'Brushing my teeth',
			completed: true,
			userId:1

		})
	})
	.then((response)=>response.json())
	.then((json)=>console.log(json))




//update a to do list item using PUT method
fetch ('https://jsonplaceholder.typicode.com/todos/1',{
		method:'PUT',
		headers: {
			'Content-Type':'application/json'
		},
		body:JSON.stringify({
			title:'Making Some Breakfast',
			description:'Breakfast is the most important meal of the day.',
			status: 'PENDING',
			dateCompleted: 'PENDING',
			userId:1

		})
	})
	.then((response)=>response.json())
	.then((json)=>console.log(json))




//update a to do list item using PATCH method
fetch ('https://jsonplaceholder.typicode.com/todos/1',{
		method:'PATCH',
		headers: {
			'Content-Type':'application/json'
		},
		body:JSON.stringify({
			status: 'completed',
			dateCompleted: '10-25-2021'

		})
	})
	.then((response)=>response.json())
	.then((json)=>console.log(json))